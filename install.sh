#!/bin/bash

# Instalador Evolution Api
# Autor: Vitor Luiz Machado
# Gitlab: https://gitlab.com/vitorluizmachado
# Version: 0.0.1-beta

# Refere-se ao Node e qual a versão que deseja instalar, basta descomentar a versão desejada
#NODE_MAJOR=16
#NODE_MAJOR=18
NODE_MAJOR=20

# Digite domínio que deseja usar
API_DOMAIN=meusite.exemplo.com.br
# Informe o E-mail para o LestEncrypt
EMAIL_CERTBOT=seu@melhoremail.com.br
# NOME DO CLIENTE 
PREFIX_NAME= evolution-api
# Gerador para GLOBAL-API-KEY
# Define os caracteres possíveis para a chave aleatória
caracteres='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
# Gera a chave aleatória de 20 caracteres
GLOBAL_API_KEY=$(head /dev/urandom | tr -dc $caracteres | head -c 20)
JWT_SECRET=$(head /dev/urandom | tr -dc $caracteres | head -c 20)

# Timezone
# Podes verificar no link https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
# Tz identifier. Ex. 'America/Cuiaba'
TIME_ZONE='America/Cuiaba'

# ------------------- NÃO ALTERAR AS LINHAS ABAIXO-------------------------
IP_Host=$(hostname -I)
echo '#############################'
echo '# Atualizando o Repositório #'
echo '#############################'
apt update
pause
clear
echo '#####################################'
echo '# Instalando Atualização do sistema #'
echo '#####################################'
apt -y upgrade
clear
echo '################################'
echo '# Instalando Essensiais Básico #'
echo '################################'
apt install -y build-essential git zip unzip nload snapd curl wget sudo chrony
apt cache
clear
echo '################################'
echo '# CONFIGURANDO SERVIDOR NTP.BR #'
echo '################################'
timedatectl set-timezone $TIME_ZONE
dpkg-reconfigure --frontend noninteractive tzdata
CONFIG_NTP='
# servidores publicos do NTP.br com NTS disponível
server a.st1.ntp.br iburst nts
server b.st1.ntp.br iburst nts
server c.st1.ntp.br iburst nts
server d.st1.ntp.br iburst nts
server gps.ntp.br iburst nts

# caso deseje pode configurar servidores adicionais com NTS, como os da cloudflare e netnod
# nesse caso basta descomentar as linhas a seguir
# server time.cloudflare.com iburst nts
# server nts.netnod.se iburst nts

# arquivo usado para manter a informação do atraso do seu relógio local
driftfile /var/lib/chrony/chrony.drift

# local para as chaves e cookies NTS
ntsdumpdir /var/lib/chrony

# se quiser um log detalhado descomente as linhas a seguir
#log tracking measurements statistics
#logdir /var/log/chrony

# erro máximo tolerado em ppm em relação aos servidores
maxupdateskew 100.0

# habilita a sincronização via kernel do real-time clock a cada 11 minutos
rtcsync

# ajusta a hora do sistema com um 'salto', de uma só vez, ao invés de
# ajustá-la aos poucos corrigindo a frequência, mas isso apenas se o erro
# for maior do que 1 segundo e somente para os 3 primeiros ajustes
makestep 1 3

# diretiva que indica que o offset UTC e leapseconds devem ser lidos
# da base tz (de time zone) do sistema
leapsectz right/UTC
'
echo '$CONFIG_NTP' > /etc/chrony/chrony.conf


echo '####################'
echo '# Instalando NodJS #'
echo '####################'
mkdir -p /etc/apt/keyrings
curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
echo 'deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main' | tee /etc/apt/sources.list.d/nodesource.list
apt install -y npm
npm install -g npm@latest
npm install -g pm2@latest
clear
echo '############################'
echo '# Baixando o Evolution API #'
echo '############################'
git clone https://github.com/EvolutionAPI/evolution-api.git
cd evolution-api
npm install
cat > src/env.yml << EOF
# ALL SETTINGS DEFINED IN THIS FILE ARE APPLIED TO ALL INSTANCES.
#
# RENAME THIS FILE TO env.yml

# Choose the server type for the application
SERVER:
  TYPE: http # https
  PORT: 8080 # 443
  URL: $API_DOMAIN

CORS:
  ORIGIN:
    - '*'
    # - yourdomain.com
  METHODS:
    - POST
    - GET
    - PUT
    - DELETE
  CREDENTIALS: true

# Install ssl certificate and replace string <domain> with domain name
# Access: https://certbot.eff.org/instructions?ws=other&os=ubuntufocal
SSL_CONF:
  PRIVKEY: /etc/letsencrypt/live/<domain>/privkey.pem
  FULLCHAIN: /etc/letsencrypt/live/<domain>/fullchain.pem

# Determine the logs to be displayed
LOG:
  LEVEL:
    - ERROR
    - WARN
    - DEBUG
    - INFO
    - LOG
    #- VERBOSE
    - DARK
    - WEBHOOKS
  COLOR: true
  BAILEYS: error # 'fatal' | 'error' | 'warn' | 'info' | 'debug' | 'trace'

# Determine how long the instance should be deleted from memory in case of no connection.
# Default time: 5 minutes
# If you don't even want an expiration, enter the value false
DEL_INSTANCE: false # or false

# Temporary data storage
STORE:
  MESSAGES: true
  MESSAGE_UP: true
  CONTACTS: true
  CHATS: true

CLEAN_STORE:
  CLEANING_INTERVAL: 7200 # 7200 seconds === 2h
  MESSAGES: true
  MESSAGE_UP: true
  CONTACTS: true
  CHATS: true

# Permanent data storage
DATABASE:
  ENABLED: false
  CONNECTION:
    URI: 'mongodb://root:root@localhost:27017/?authSource=admin&readPreference=primary&ssl=false&directConnection=true'
    DB_PREFIX_NAME: $PREFIX_NAME # PREFIX NAME
  # Choose the data you want to save in the application's database or store
  SAVE_DATA:
    INSTANCE: false
    NEW_MESSAGE: false
    MESSAGE_UPDATE: false
    CONTACTS: false
    CHATS: false

REDIS:
  ENABLED: false
  URI: 'redis://localhost:6379'
  PREFIX_KEY: $PREFIX_NAME  # PREFIX NAME

RABBITMQ:
  ENABLED: false
  URI: 'amqp://guest:guest@localhost:5672'

WEBSOCKET:
  ENABLED: false

# Global Webhook Settings
# Each instance's Webhook URL and events will be requested at the time it is created
WEBHOOK:
  # Define a global webhook that will listen for enabled events from all instances
  GLOBAL:
    URL: <url>
    ENABLED: false
    # With this option activated, you work with a url per webhook event, respecting the global url and the name of each event
    WEBHOOK_BY_EVENTS: false
  # Automatically maps webhook paths
  # Set the events you want to hear
  EVENTS:
    APPLICATION_STARTUP: false
    QRCODE_UPDATED: true
    MESSAGES_SET: true
    MESSAGES_UPSERT: true
    MESSAGES_UPDATE: true
    MESSAGES_DELETE: true
    SEND_MESSAGE: true
    CONTACTS_SET: true
    CONTACTS_UPSERT: true
    CONTACTS_UPDATE: true
    PRESENCE_UPDATE: true
    CHATS_SET: true
    CHATS_UPSERT: true
    CHATS_UPDATE: true
    CHATS_DELETE: true
    GROUPS_UPSERT: true
    GROUP_UPDATE: true
    GROUP_PARTICIPANTS_UPDATE: true
    CONNECTION_UPDATE: true
    CALL: true
    # This event fires every time a new token is requested via the refresh route
    NEW_JWT_TOKEN: false

CONFIG_SESSION_PHONE:
  # Name that will be displayed on smartphone connection
  CLIENT: $PREFIX_NAME
  NAME: chrome # chrome | firefox | edge | opera | safari

# Set qrcode display limit
QRCODE:
  LIMIT: 30
  COLOR: "#198754"

# Defines an authentication type for the api
# We recommend using the apikey because it will allow you to use a custom token,
# if you use jwt, a random token will be generated and may be expired and you will have to generate a new token
AUTHENTICATION:
  TYPE: apikey # jwt or apikey
  # Define a global apikey to access all instances
  API_KEY:
    # OBS: This key must be inserted in the request header to create an instance.
    KEY: $GLOBAL_API_KEY
  # Expose the api key on return from fetch instances
  EXPOSE_IN_FETCH_INSTANCES: true
  # Set the secret key to encrypt and decrypt your token and its expiration time.
  JWT:
    EXPIRIN_IN: 0 # seconds - 3600s === 1h | zero (0) - never expires
    SECRET: $JWT_SECRET
EOF
clear
echo '#######################################################'
echo 'Geramos um arquivo em /root/evolution-api/key/Key.txt #'
echo 'GLOBAL API KEY: '$GLOBAL_API_KEY                     '#'
echo 'JWT SECRETE: '$JWT_SECRET                            '#'
echo '#######################################################'

cat > '/root/evolution-api/key/Key.txt' << EOF
GLOBAL API KEY: $GLOBAL_API_KEY
JWT SECRETE: $JWT_SECRET
EOF
clear
echo '#######################################'
echo '# Configurando a Inicialização da API #'
echo '#######################################'
pm2 start 'npm run start:prod' --name ApiEvolution # nome que vai no pm2
pm2 startup
pm2 save --force
clear
echo '####################################'
echo '# CONFIGURAÇÃO PROXY REVERSO NGINX #'
echo '####################################'
apt install -y nginx
systemctl start nginx
systemctl enable nginx
systemctl status nginx
echo '##################################################'
echo '# Teste Nginx clicando aqui => http://'$IP_Host' #' 
echo '##################################################'
read -rsn1 -p'Precione qualquer tecla para continuar'; echo
echo '####################################'
echo '# CONFIGURAÇÃO PROXY REVERSO NGINX #'
echo '####################################'
snap install --classic certbot
rm /etc/nginx/sites-enabled/default
cat > /etc/nginx/conf.d/default.conf << EOF
server {
  listen 80;
  listen [::]:80;
  server_name _;
  root /var/www/html/;
  index index.php index.html index.htm index.nginx-debian.html;
  location / {
    try_files \$uri \$uri/ /index.php;
  }

  location ~ \.php$ {
    fastcgi_pass unix:/run/php/php7.4-fpm.sock;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    include fastcgi_params;
    include snippets/fastcgi-php.conf;
  }

  # A long browser cache lifetime can speed up repeat visits to your page
  location ~* \.(jpg|jpeg|gif|png|webp|svg|woff|woff2|ttf|css|js|ico|xml)$ {
       access_log        off;
       log_not_found     off;
       expires           360d;
  }

  # disable access to hidden files
  location ~ /\.ht {
      access_log off;
      log_not_found off;
      deny all;
  }
}
EOF

nginx -t
systemctl reload nginx
chown www-data:www-data /usr/share/nginx/html -R
cd ~
cat > /etc/nginx/sites-available/api <<EOF
server {
  server_name $API_DOMAIN;

  location / {
    proxy_pass http://127.0.0.1:8080;
    proxy_http_version 1.1;
    proxy_set_header Upgrade \$http_upgrade;
    proxy_set_header Connection 'upgrade';
    proxy_set_header Host \$host;
    proxy_set_header X-Real-IP \$remote_addr;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_cache_bypass \$http_upgrade;
  }
}
EOF

ln -s /etc/nginx/sites-available/api /etc/nginx/sites-enabled
nginx -t
systemctl reload nginx

clear
echo '##################################'
echo '# CONFIGURAÇÃO DO SSL NO DOMÍNIO #'
echo '##################################'
certbot --nginx -d $API_DOMAIN --non-interactive --agree-tos -m $EMAIL_CERTBOT

ls -l /etc/letsencrypt/live/$API_DOMAIN/

clear
echo '##############################################'
echo '# SSL INSTALADO ACESSE 'https://$API_DOMAIN '#'
echo '##############################################'
echo '#             SALVE SUAS KEY                 #'
echo '##############################################'
echo '# GLOBAL API KEY:'$GLOBAL_API_KEY           '#'
echo '#                                            #'
echo '# JWT API KEY:'$JWT_SECRET                  '#'
echo '#                                            #'
echo '##############################################'
echo '#############################################'
echo '#       Qualquer ajuda é bem vinda         #!'
echo '#                                           #'
echo '#     pix: vitorluizmachado@gmai.com       #!'
echo '#############################################'
